import {translationsOfCsv, VueI18n} from '../dist/csv-i18n/csv-i18n.js';
import Vue from 'vue';
import translations from './translations.csv';

Vue.use(VueI18n(translationsOfCsv(translations), 'fr-FR'));

const supportedLocales = ['en-US', 'fr-FR'];
const component = {
  methods: {
    changeLocale() {
      const currentIdx = supportedLocales.indexOf(this.getCurrentLocale());
      const next = (currentIdx + 1) % supportedLocales.length;
      this.setCurrentLocale(supportedLocales[next]);
    }
  },
  render(h) {
    return h('div', [
      h('button', {on: {click: this.changeLocale}}, 'Change Locals'),
      h('div', this.t('Bonjour {0} {1}', 'Titouan', 'CREACH'))
    ]);
  }
};

new Vue({
  render(h) {
    return h('div', [h('div', this.t('Intégration de donnée')), h(component)]);
  }
}).$mount('#app');
