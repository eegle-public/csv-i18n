const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry:  './example/example.js',
  mode: 'development',
  output: {
    filename: './example/app.bundle.js'
  },
  module: {
    rules: [{
      test: /\.(c|d|t)sv$/,
      use: ['dsv-loader?delimiter=;']
    }]
  },

  devtool: 'eval',

  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      template: 'example/index.html'
    })
  ]
};
