# i18n-csv

Simple i18n plugin for Vue and javascript, configurable via CSV.

## Summary
i18n-csv export 3 functions:
  - `createI18n` that return an instance of the plugin
  - `translationsOfCsv` that create a compatible "Translations" object from an imported CSV using this [plugin](https://github.com/wbkd/dsv-loader) (for convenience) 
  - `VueI18n` that create a Vue plugin

## createI18n

Create an i18n instance

```flow

// with
type StringToTranslate = string;
type LocaleName = string;
type TranslationResult = string;


// we define
type Translations = {
  [StringToTranslate]: {
    [LocalName]: TranslationResult,
    [LocalName]: TranslationResult
  }
}

type I18n = T; // we define this later, keep reading the doc :)

// Translations example:

const myTraductionsObject: Translations = {
  'Hello': {
    'en-US': 'Hello',
    'fr-FR': 'Bonjour'
  }
};


function createI18n(translations: Translations, defaultLocale: localeName): I18n
```

## I18n instance
The object returned by `createI18n` is immutable by design. 
It lets the caller to manage how the mutability should operate if any (Reactive for Vue or whatever).
It exposes 3 methods:

```javascript
type I18n = {
  translate: (string, ...string) => string;
  getCurrentLocale: () => LocaleName;
  setCurrentLocale: (LocaleName) => I18n;
}

// note that there is an alias "_t" to translate

const instance = createI18n(traductions, 'fr-FR');

instance._t('String', ...args); // or instance.translate
instance.getCurrentLocale()
const newInstance = instance.setCurrentLocale('en-US');
newInstance.translate('String', ...args);

```


## translationsOfCsv
Convert the output of dsv-loader to our `Translations` object

```javascript
type Csv = [
   {localeName: traduction}, // csv Line
   {localeName: traduction},
   column: localeName[]
]


function translationsOfCsv(Csv): Translations;
```  

## VueI18n
Create a Vue plugin.

```javascript


type VuePlugin = T
function VueI18n(...args): VuePlugin;
```

`...args` are the parameters passed to `createI18n`

The Vue plugin will inject 3 methods into the vue instance. These methods depends of reactive data.
The methods, attached the instance Vue instance are:
```javascript
  getCurrentLocale(): LocaleName;
  setCurrentLocale(LocaleName); // reactively update vue instances that depends of i18n
  translate(string, ...string): string // translate
```
Note that we aliased the instance methods `t` to translate.


## CSV Format

|locale1      |locale2      |locale3      |
|-------------|-------------|-------------|
|Translation 1|Translation 2|Translation 3|

Note that `locale1` is used as default locale.

Translation can use placeholder example:

|fr-FR        | en-US        |
|-------------|--------------|
| Boujour {0} | {0}, Hello ! |


You can call the translation via the `_t` or `translate` function.

```javascript
_t("Bonjour {0}", "Eegle")
```

will produce

`"Bonjour Eegle"` if locale is set to "fr-FR". If locale is set to "en-US" it will output:
`"Eegle, Hello !"`. If locale is set to "es-ES", it will output the default string: `Bonjour Eegle` 


## Import CSV format
Importing CSV format will require a plugin in order to parse the CSV file.
We recommand https://github.com/wbkd/dsv-loader.

### Example of `webpack.config.js` :

```javascript
  module: {
    rules: [{
      test: /\.(c|d|t)sv$/,
      use: ['dsv-loader?delimiter=;']
    }]
  },
```

We can now call the plugin with the i18n converter:
```
import Vue from 'vue';
import translations from './translations.csv';
import {translationsOfCsv, VueI18n, createI18n} from './index.js';

// for Vue
VueI18n(translationsOfCsv(translations), 'fr-FR');

new Vue({
  render(h) {
      return h('span', 'My current locale is: ', this.getCurrentLocale())
  }
    
}).$moun('#app');

// Will output:
// <span> My current locale is: 'fr-FR' </span>

// for javascript
const translator = createI18n(translationsOfCsv(translations), 'fr-FR');
console.log('My current locale is: ' + translator.getCurrentLocale());
// Will output
// My current locale is: 'fr-FR'

```

## Developpers

## Build the package:

```
yarn run build
```

## Run the example with a dev server:

```
yarn run example
```
Note that the example use the compiled version of the plugin. You may recompile the
package using the commande above is you do any edit to the plugin itself.

## Format the code:

```
yarn run format
```
 Format the sources and the example.
 Should be run before commiting changes
 

## Analyze the size

```
yarn run analyze
```
Analyze the size of the output plugin. 