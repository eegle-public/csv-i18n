module.exports = {
  entry:  './src/index.js',
  mode: 'production',
  output: {
    filename: "./csv-i18n/csv-i18n.js",
    library: 'csv-i18n',
    libraryTarget: 'umd'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};
