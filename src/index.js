const interpolator = args => {
  return (_, n) => args[n];
};

export const translationsOfCsv = x => {
  const {columns} = x;

  return x
    .map(y => {
      return [
        y[columns[0]],
        {
          ...y
        }
      ];
    })
    .reduce((acc, [k, v]) => ({...acc, [k]: v}), {});
};

export function createI18n(translations, defaultLocale) {
  return {
    locale: defaultLocale,
    translate(s, ...args) {
      return (
        (translations &&
          translations[s] &&
          translations[s][this.locale] &&
          translations[s][this.locale].replace(/{(\d*)}/g, interpolator(args))) ||
        s
      );
    },
    _t(...args) {
      return this.translate(...args);
    },
    setCurrentLocale(locale) {
      return createI18n(translations, locale);
    },
    getCurrentLocale() {
      return this.locale;
    }
  };
}

export function VueI18n(...args) {
  return {
    install(Vue) {
      // we make global data reactive
      const vm = new Vue({
        data: {
          i18n: createI18n(...args)
        }
      });

      Vue.mixin({
        methods: {
          t(...args) {
            return vm.i18n.translate(...args);
          },
          translate(...args) {
            return vm.i18n.translate(...args);
          },
          setCurrentLocale(...args) {
            vm.i18n = vm.i18n.setCurrentLocale(...args);
            return this;
          },
          getCurrentLocale(...args) {
            return vm.i18n.getCurrentLocale(...args);
          }
        }
      });
    }
  };
}
